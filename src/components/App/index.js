import React, { Component } from "react";
import Main from "../Main";
import "./App.css";
import Intro from "../Intro";
import "whatwg-fetch";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Tv Series List</h1>
          <Intro message="test message" />
        </header>
        <Main />
      </div>
    );
  }
}

export default App;
