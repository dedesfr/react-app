import React, { Component } from "react";
import SeriesList from "../../components/SeriesList";
import Loader from "../../components/Loader";

class Series extends Component {
  constructor() {
    super();
    this.state = {
      series: [],
      seriesName: "",
      isFetching: false
    };
  }

  //   componentDidMount() {
  //     fetch("http://api.tvmaze.com/search/shows?q=girls")
  //       .then(response => response.json())
  //       .then(json => this.setState({ series: json }));
  //     // const series = ["Vikings", "Arrow", "The Walking Dead"];

  //     // setTimeout(() => {
  //     //   this.setState({ series: series });
  //     // }, 2000);
  //   }

  onSeriesInputChange = e => {
    this.setState({ seriesName: e.target.value, isFetching: true });
    fetch(`http://api.tvmaze.com/search/shows?q=${e.target.value}`)
      .then(response => response.json())
      .then(json => this.setState({ series: json, isFetching: false }));
  };

  render() {
    const { series, seriesName, isFetching } = this.state;

    return (
      <div>
        The length of this series - {this.state.series.length}
        <div>
          <input type="text" onChange={this.onSeriesInputChange} />
        </div>
        {!isFetching &&
          series.length === 0 &&
          seriesName.trim() === "" && <p>Please enter value</p>}
        {!isFetching &&
          series.length === 0 &&
          seriesName.trim() !== "" && <p>Judul film ga ditemukan fellas</p>}
        {isFetching && <Loader />}
        {!isFetching && <SeriesList list={this.state.series} />}
      </div>
    );
  }
}

export default Series;
