import React, { Component } from "react";
import Loader from "../../components/Loader";
import { Link } from "react-router-dom";

class SingleSeries extends Component {
  
  state = {
    show: null
  };
  componentDidMount() {
    const { id } = this.props.match.params;
    fetch(`http://api.tvmaze.com/shows/${id}?embed=cast`)
      .then(response => response.json())
      .then(json =>
        this.setState({
          show: json
        })
      );
  }

  render() {
    const { show } = this.state;
    console.log(show);
    return (
      <div>
        {show === null && <Loader />}
        {show !== null && (
          <div>
            <p>Show name {show.name}</p>
            <p>Show rating {show.rating.average}</p>
            <ul>
              {show.genres.map(genre => (
                <li key={genre}>{genre}</li>
              ))}
            </ul>
            <p>
              <img alt="show" src={show.image.medium} />
            </p>
          </div>
        )}
        <Link to={`/`}>Go Bek</Link>
      </div>
    );
  }
}

export default SingleSeries;
